local config = require("lapis.config")

config({"development", "production"}, {
    port = "80",
    dns_server = "127.0.0.11",

    num_workers = "2",

    card_service_url = 'http://card-service/terminal_api/',
    web_interface_url = 'http://web-interface/terminal_api/',

    redis = {
        host = "redis",
        port = 6379,
        db = 0
    },

    email_enabled = false,

    code_cache = "off",
})

config("production", {
    email_enabled = true,
    code_cache = "on"
})